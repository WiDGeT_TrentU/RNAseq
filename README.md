```Daphnia RNAseq projects```

Patterns of Gene Expression in Daphnia Experiencing Dietary Stress

CITATION

<b>CONTENTS OF DAPHNIA_RNASEQ_PROJECTS<b>

1.  Data analysis scripts from Catriona Jones (PhD 2022 - Paul Frost lab); 

Jones et al. 2020 PRSB 287: 20202302

Jones et al. 2021 Submitted

2.  Outdated (data analysis scripts from William Kim (2018 USRA summer student))
