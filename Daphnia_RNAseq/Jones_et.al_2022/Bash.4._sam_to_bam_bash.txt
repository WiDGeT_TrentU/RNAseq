#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --ntasks=8
#SBATCH --time=0-2
module load samtools/1.5
samtools view -t 8 -h CT10.sam > CT10.bam
samtools view -t 8 -h CT2.sam > CT2.bam
samtools view -t 8 -h CT3.sam > CT3.bam
samtools view -t 8 -h CT5.sam > CT5.bam
samtools view -t 8 -h CT8.sam > CT8.bam
samtools view -t 8 -h FA2.sam > FA2.bam
samtools view -t 8 -h FA3.sam > FA3.bam
samtools view -t 8 -h FA5.sam > FA5.bam
samtools view -t 8 -h FA6.sam > FA6.bam
samtools view -t 8 -h FA7.sam > FA7.bam
samtools view -t 8 -h LCa1.sam > LCa1.bam
samtools view -t 8 -h LCa2.sam > LCa2.bam
samtools view -t 8 -h LCa3.sam > LCa3.bam
samtools view -t 8 -h LCa7.sam > LCa7.bam
samtools view -t 8 -h LCa8.sam > LCa8.bam
samtools view -t 8 -h LF1.sam > LF1.bam
samtools view -t 8 -h LF2.sam > LF2.bam
samtools view -t 8 -h LF4.sam > LF4.bam
samtools view -t 8 -h LF5.sam > LF5.bam
samtools view -t 8 -h LF7.sam > LF7.bam
samtools view -t 8 -h LN1.sam > LN1.bam
samtools view -t 8 -h LN4.sam > LN4.bam
samtools view -t 8 -h LN5.sam > LN5.bam
samtools view -t 8 -h LN6.sam > LN6.bam
samtools view -t 8 -h LN8.sam > LN8.bam
samtools view -t 8 -h LP1.sam > LP1.bam
samtools view -t 8 -h LP3.sam > LP3.bam
samtools view -t 8 -h LP5.sam > LP5.bam
samtools view -t 8 -h LP6.sam > LP6.bam
samtools view -t 8 -h LP7.sam > LP7.bam
end
