library(tidyverse)
library(KOGMWU)

LFC <- read.csv("JustLogFoldChange.csv")

Ca <- select(LFC, Ca.Mean)
Cy <- select(LFC, CY.Mean)
Fd <- select(LFC, C.Mean)
Nt <- select(LFC, N.Mean)
Ph <- select(LFC, P.Mean)

gene2kog <- read.csv("KOGclass.csv")

CaMWU <- kog.mwu(Ca, gene2kog, Alternative = "t")
CyMWU <- kog.mwu(Cy, gene2kog, Alternative = "t")
FdMWU <- kog.mwu(Fd, gene2kog, Alternative = "t")
NtMWU <- kog.mwu(Nt, gene2kog, Alternative = "t")
PhMWU <- kog.mwu(Ph, gene2kog, Alternative = "t")

ktable=makeDeltaRanksTable(list("Calcium"= CaMWU,"Cyanobacteria"= CyMWU,"Carbon"= FdMWU,
                               "Nitrogen"= NtMWU, "Phosphorus"= PhMWU))

# Making a heatmap with hierarchical clustering trees: 
pheatmap(as.matrix(ktable),clustering_distance_cols="correlation") 

write.csv(ktable,"C:\\Users\\Frost Xenopolous Lab\\Desktop\\KOG_ktable.csv", row.names = TRUE)



