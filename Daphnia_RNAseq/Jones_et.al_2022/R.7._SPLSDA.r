library(mixOmics)

X <- read.csv("SPLSDA X attributes.csv", row.names=1)  
Y <- read.csv("SPLSDA Y attributes.csv", row.names=1)
X <- as.matrix(X)
Y <- as.factor(Y[,1])

set.seed(2935)

list.keepX <- c(seq(10, 100, 10))
# to speed up the computational time, consider the cpu argument
# take ~ 4 min to run - train the model and get tuning parameters for final model
tune.splsda <- tune.splsda(X, Y, ncomp = 10, validation = "Mfold", folds = 4, 
                           progressBar = FALSE, dist = "max.dist",
                           test.keepX = list.keepX, nrepeat = 10) #nrepeat 50-100 for better estimate
choice.ncomp <- tune.splsda$choice.ncomp$ncomp
choice.keepX <- tune.splsda$choice.keepX[1:choice.ncomp]

Final.SPLSDA <- splsda(X,Y, ncomp = choice.ncomp, keepX = choice.keepX)  

pred <- vector()
for (i in 1:30){
  pred[i] <- predict(splsda(X[-i,],Y[-i], ncomp = choice.ncomp, 
                            keepX = choice.keepX, near.zero.var = TRUE), t(X[i,]),
                     dist = "max.dist")$class$max.dist[,4]
}

# calculate the error rate of the model
confusion.mat = get.confusion_matrix(truth = Y, predicted = pred)
get.BER(confusion.mat)
confusion.mat

Chosen.Variables <- selectVar(Final.SPLSDA, comp = 5)$value

write.csv(Chosen.Variables,"C:\\Users\\Frost Xenopolous Lab\\Documents\\Cat's Documents\\Uni Stuff\\PhD\\Papers in progress\\Nutritional profiling using transcriptomics\\Datasets\\SPLSDA Chosen Variables.csv")
