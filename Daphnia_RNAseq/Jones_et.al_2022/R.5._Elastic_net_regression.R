library(glmnet)
library(caret)
library(MASS)
library(tidyverse)


rm(list=ls())

#Read in datasets
X <- (read.csv("glmnet X attributes.csv"))
Y <- (read.csv("Treatment data scaled.csv"))


#Remove 'treatment' column and convert to numeric matrix
Y <- select(Y,-Treatment)
Y <- as.matrix(Y)

X <- select(X,-Treatment)
X <- as.matrix(X)

#######################################################################################
#################################### MULTIVARIATE MODEL #################################
#######################################################################################
set.seed(98697)

#This is the multivariate model which considers all 5 treatments and therefore uses 
#the 'mgaussian' family

fit.elnet <- glmnet(X, Y, family='mgaussian', alpha=.5)

# 10-fold Cross validation for each alpha = 0, 0.1, ... , 0.9, 1.0
for (i in 0:10) {
  assign(paste("fit", i, sep=""), cv.glmnet(X, Y, type.measure="mse", 
                                            alpha=i/10,family='mgaussian'
  ))
}

#Select an optimum alpha and lambda
par(mfrow=c(2,5))
plot(fit1);plot(fit2);plot(fit3);plot(fit4);plot(fit5);plot(fit6);
plot(fit7);plot(fit8);plot(fit9);plot(fit10)

par(mfrow=c(1,1))
plot(log(fit1$lambda),fit1$cvm,pch=19,col="red",xlab="log(Lambda)",ylab=fit1$name)
points(log(fit3$lambda),fit3$cvm,pch=19,col="grey")
points(log(fit5$lambda),fit5$cvm,pch=19,col="blue")
points(log(fit7$lambda),fit7$cvm,pch=19,col="orange")
points(log(fit9$lambda),fit9$cvm,pch=19,col="green")
legend("topleft",legend=c("alpha= 0.1", "alpha= .3", "alpha= .5", "alpha= .7", "alpha= .9"),pch=19,col=c("red", "orange", "grey", "green", "blue"))


#Get the optimum lambda and number of genes with optimum lambda - use lambda 1se rather
#than minimum lambda. Fit5 is the model with the lowest MSE and lowest
#penalty (lambda) - see plots above. Optimum alpha = 0.4, optimal lambda = 0.71
fit1$index
fit1$lambda.1se
fit2$index
fit2$lambda.1se
fit3$index
fit3$lambda.1se
fit4$index
fit4$lambda.1se
fit5$index
fit5$lambda.1se
fit6$index
fit6$lambda.1se
fit7$index
fit7$lambda.1se
fit8$index
fit8$lambda.1se
fit9$index
fit9$lambda.1se
fit10$index
fit10$lambda.1se

#Get mean square error for cross-validation
fit5$cvm[fit5$lambda == fit5$lambda.min]
# MSE min = 1.604

#Optimum lambda = 0.34
#Genes with optimum lambda = 128

#Plot the coefficients and lambdas - the first plot has one per nutrient, the second plot
#is just singular
par(mfrow=c(2,3))
plot(fit.elnet, xvar="lambda")
plot(fit5, main= "Optimum lambda = 0.71, 128 genes with optimal lambda \n")

#Export as .tiff with dimensions 1500*770

#Finally, get the coefficients for the model - AKA which genes were retained
#and which were dropped
Coefficients <- coef(fit5, s = fit5$lambda.1se)
P.coef <- as.matrix(Coefficients$CP)
N.coef <- as.matrix(Coefficients$CN)
C.coef <- as.matrix(Coefficients$C)
Ca.coef <- as.matrix(Coefficients$Ca)
Cy.coef <- as.matrix(Coefficients$Cyano)

write.csv(P.coef,"C:\\Users\\Frost Xenopolous Lab\\Documents\\Cat's Documents\\Uni Stuff\\PhD\\Papers in progress\\Nutritional profiling using transcriptomics\\GLMnet\\P.csv",
          row.names = TRUE)

write.csv(N.coef,"C:\\Users\\Frost Xenopolous Lab\\Documents\\Cat's Documents\\Uni Stuff\\PhD\\Papers in progress\\Nutritional profiling using transcriptomics\\GLMnet\\N.csv",
          row.names = TRUE)

write.csv(C.coef,"C:\\Users\\Frost Xenopolous Lab\\Documents\\Cat's Documents\\Uni Stuff\\PhD\\Papers in progress\\Nutritional profiling using transcriptomics\\GLMnet\\C.csv",
          row.names = TRUE)

write.csv(Ca.coef,"C:\\Users\\Frost Xenopolous Lab\\Documents\\Cat's Documents\\Uni Stuff\\PhD\\Papers in progress\\Nutritional profiling using transcriptomics\\GLMnet\\Ca.csv",
          row.names = TRUE)

write.csv(Cy.coef,"C:\\Users\\Frost Xenopolous Lab\\Documents\\Cat's Documents\\Uni Stuff\\PhD\\Papers in progress\\Nutritional profiling using transcriptomics\\GLMnet\\Cy.csv",
          row.names = TRUE)

#
