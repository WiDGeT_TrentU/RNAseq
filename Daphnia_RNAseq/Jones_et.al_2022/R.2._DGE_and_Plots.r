source("https://bioconductor.org/biocLite.R")
install.packages(c("RColorBrewer","mixOmics"))
biocLite("edgeR")
biocLite("limma")
install.packages("stringi")
install.packages('ggfortify')
install.packages("cluster")
install.packages("FactoMineR")
library(ggplot2)
library(reshape2)
library(mixOmics)
library(RColorBrewer)
library(gridExtra)
library(edgeR)
library(devtools)
library(dplyr)


#your folder may be in a different location
rawCountTable <- read.table("feature_counts.txt", header=TRUE, sep="\t", row.names=1)
sampleInfo <- read.table("Main_design_file.csv", header=TRUE, sep=",", row.names= 1)
head(rawCountTable)
nrow(rawCountTable)

countData <- select(rawCountTable, -Chr, -Start, -End, -Strand, -Length)
countData <- rename(countData, CT2 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_CT2.bam",
                    CT3 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_CT3.bam",
                    CT5 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_CT5.bam",
                    CT8 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_CT8.bam",
                    CT10 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_CT10.bam",
                    FA2 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_FA2.bam",
                    FA3 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_FA3.bam",
                    FA5 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_FA5.bam",
                    FA6 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_FA6.bam",
                    FA7 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_FA7.bam",
                    LCa1 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LCa1.bam",
                    LCa2 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LCa2.bam",
                    LCa3 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LCa3.bam",
                    LCa7 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LCa7.bam",
                    LCa8 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LCa8.bam",
                    LF1 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LF1.bam",
                    LF2 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LF2.bam",
                    LF4 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LF4.bam",
                    LF5 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LF5.bam",
                    LF7 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LF7.bam",
                    LN1 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LN1.bam",
                    LN4 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LN4.bam",
                    LN5 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LN5.bam",
                    LN6 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LN6.bam",
                    LN8 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LN8.bam",
                    LP1 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LP1.bam",
                    LP3 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LP3.bam",
                    LP5 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LP5.bam",
                    LP6 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LP6.bam",
                    LP7 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LP7.bam"
)   
      
 

###summarizedCountData <- dplyr::summarise_all(countData, funs(mean))

#We then use the new dataframe to build the DGE
dgeFull <- DGEList(countData, group=sampleInfo$Treatment)
dgeFull

#We want to log transform our data BUT there are alot of 0s in our dgeFull counts 
#What happens when you take try log2(0) This is why we add +1 (pseudocounts)
pseudoCounts <- log2(dgeFull$counts+1)

design <- model.matrix(~dgeFull$samples$lib.size)
dgeFull <- calcNormFactors(dgeFull)
dgeFull <- estimateDisp(dgeFull, design = design)

fit <- glmFit(dgeFull, design)
lrt <- glmLRT(fit, coef=ncol(design))
tt <- topTags(lrt, n=nrow(dgeFull), p.value=1)
tt
#This is where I apply the BH adjustment to all rows of the tt dataset and output a new adjusted dataset called TT
TT <- topTags(tt, n=nrow(dgeFull), sort.by="PValue", p.value=0.5)
TT
install.packages("openxlsx")
library(openxlsx)
openxlsx::write.xlsx(pseudoCounts, "C:/Users/Daisy/Documents/Uni Stuff/PhD/Papers in progress/Nutritional profiling using transcriptomics/BioInformatics/Data/pseudoCounts.xlsx",
                     row.names=TRUE)
openxlsx::write.xlsx(tt, "C:/Users/Daisy/Documents/Uni Stuff/PhD/Papers in progress/Nutritional profiling using transcriptomics/BioInformatics/Data/Top tags.xlsx",
                     row.names=TRUE)
