###################################################################################
##Non-parametric nearest neighbour model (K-nn) - uses Class and caret packages###
###################################################################################

#Load packages
library(class)
library(caret)
library(tidyverse)
library(gmodels)

####################################################################################
#########################Knn model for glmnet dataset###############################
###################################################################################
#Read in data
ENR <- read.csv('ENR genes.csv')

#Training the model
set.seed(123)
ENR.tc <- trainControl(method="repeatedcv",repeats = 50, classProbs = TRUE) 
knnFitENR <- train(Treatment ~ ., data = ENR, method = "knn", 
                   trControl = ENR.tc, tuneLength = 20)

xtab <- table(predict(knnFitENR, ENR),ENR$Treatment)
confusionMatrix(xtab)

#Read in data
BM <- read.csv('Biomarkers genes.csv')

#Training the model
set.seed(123)
knnFitBM <- train(Treatment ~ ., data = BM, method = "knn", 
                    trControl = ENR.tc, tuneLength = 20)

xtab <- table(predict(knnFitBM, BM),BM$Treatment)
confusionMatrix(xtab)
