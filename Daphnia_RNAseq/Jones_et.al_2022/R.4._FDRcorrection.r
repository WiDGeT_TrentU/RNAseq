rm(list = ls())
source("https://bioconductor.org/biocLite.R")
biocLite("qvalue")
library(qvalue)
library(tidyverse)

GeneList <- read.csv("Gene List.csv")
CT <- GeneList$Control.P.Values
Ca <- GeneList$Calcium.P.Values
FA <- GeneList$Fatty.Acid.P.Values
LF <- GeneList$Carbon.P.Values
LN <- GeneList$Nitrogen.P.Values
LP <- GeneList$Phosphorus.P.Values
head(GeneList)

hist(pvalues, nclass=100)
  
FDRadjCT <- p.adjust(CT, method = "fdr")
FDRadjCa <- p.adjust(Ca, method = "fdr")
FDRadjFA <- p.adjust(FA, method = "fdr")
FDRadjLF <- p.adjust(LF, method = "fdr")
FDRadjLN <- p.adjust(LN, method = "fdr")
FDRadjLP <- p.adjust(LP, method = "fdr")

library(xlsx)
write.xlsx(FDRadjCT, "C:/Users/Daisy/Documents/Uni Stuff/PhD/BioInformatics/Data/FDRadjCT.xlsx")
write.xlsx(FDRadjCa, "C:/Users/Daisy/Documents/Uni Stuff/PhD/BioInformatics/Data/FDRadjCA.xlsx")
write.xlsx(FDRadjFA, "C:/Users/Daisy/Documents/Uni Stuff/PhD/BioInformatics/Data/FDRadjFA.xlsx")
write.xlsx(FDRadjLF, "C:/Users/Daisy/Documents/Uni Stuff/PhD/BioInformatics/Data/FDRadjLF.xlsx")
write.xlsx(FDRadjLN, "C:/Users/Daisy/Documents/Uni Stuff/PhD/BioInformatics/Data/FDRadjLN.xlsx")
write.xlsx(FDRadjLP, "C:/Users/Daisy/Documents/Uni Stuff/PhD/BioInformatics/Data/FDRadjLP.xlsx")

