#Clear memory first
rm(list = ls())

#Load packages
source("https://bioconductor.org/biocLite.R")
biocLite("edgeR")
install.packages("ggfortify")
install.packages("lmSupport")
library(edgeR)
library(dplyr)
library(ggfortify)
library(lmSupport)
#-----------------------------------------------------------------------------------------
rawCountTable <- read.table("feature_counts.txt", header=TRUE, sep="\t", row.names=1)
sampleInfo <- read.table("Main_design_file.csv", header=TRUE, sep=",", row.names= 1)
head(rawCountTable)
nrow(rawCountTable)

countData <- select(rawCountTable, -Chr, -Start, -End, -Strand, -Length)
countData <- rename(countData, CT2 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_CT2.bam",
                    CT3 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_CT3.bam",
                    CT5 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_CT5.bam",
                    CT8 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_CT8.bam",
                    CT10 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_CT10.bam",
                    FA2 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_FA2.bam",
                    FA3 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_FA3.bam",
                    FA5 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_FA5.bam",
                    FA6 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_FA6.bam",
                    FA7 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_FA7.bam",
                    LCa1 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LCa1.bam",
                    LCa2 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LCa2.bam",
                    LCa3 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LCa3.bam",
                    LCa7 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LCa7.bam",
                    LCa8 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LCa8.bam",
                    LF1 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LF1.bam",
                    LF2 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LF2.bam",
                    LF4 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LF4.bam",
                    LF5 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LF5.bam",
                    LF7 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LF7.bam",
                    LN1 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LN1.bam",
                    LN4 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LN4.bam",
                    LN5 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LN5.bam",
                    LN6 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LN6.bam",
                    LN8 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LN8.bam",
                    LP1 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LP1.bam",
                    LP3 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LP3.bam",
                    LP5 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LP5.bam",
                    LP6 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LP6.bam",
                    LP7 = "X.home.jonesclc.projects.rrg.shaferab.jonesclc.Nutrigenomics.sorted_LP7.bam"
)   



#---------------------------------------------------------------------------------------

#Test glm on one gene and run diagnostics

gene1 <- as.numeric(countData["DAPPUDRAFT_309027",])

#Generalised linear regression with a poisson link function
glm_poi <- glm(gene1 ~ Treatments,family=c("poisson"), data=countData)

summary(glm_poi)
#Plot the residuals
opar <- par(mfrow = c(2,2), oma = c(0, 0, 1.1, 0))
plot(glm_poi, las = 1)
autoplot(glm_poi)

#--------------------------------------------------------------------------------
#Power analysis

modelPower(pc = 5, pa = 4, N = 10, alpha = 0.05, power = NULL, peta2=0.3025)
install.packages("pwr")
library(pwr)
pwr.f2.test(u = 12, v = , f = 0.35, sig.level = 0.05, power = 0.8)
#-----------------------------------------------------------------------------------------
#Link function to perform glm on all ~25000 genes then print summary output to 
#a .txt file
loop_list=as.factor(row.names(countData))
loop_list

Treatments=as.factor(c("CTR","CTR","CTR","CTR","CTR","LFA",
                       "LFA", "LFA", "LFA", "LFA",
                       "LCA","LCA","LCA","LCA","LCA",
                       "LFD","LFD","LFD","LFD","LFD","LNR",
                       "LNR","LNR","LNR","LNR", "LPH",
                       "LPH","LPH","LPH","LPH"))


for(k in loop_list){
  gene <- as.numeric(countData[k,])
      k <- glm(gene ~ Treatments, family = poisson)
  capture.output(summary(k), file=paste0("glm.txt"), append=TRUE)
}    


#To get gene list to match up in excel
library(xlsx)
write.xlsx(loop_list, "C:/Users/Daisy/Documents/Uni Stuff/PhD/BioInformatics/Data/Gene List.xlsx")

#To calculate false discovery rate
library(fdrtool)
pvalues <- read.csv("pvalues.csv")
padj <- p.adjust(pvalues, method="BH", n=length(pvalues))

