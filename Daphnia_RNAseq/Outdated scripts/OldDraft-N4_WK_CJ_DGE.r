#Most of below will work without the below packages; the fun heatmap requires most of below
#Might require Xquartz and gcc (Fortran) installation on Mac 
#possibly: xcode-select --install
source("https://bioconductor.org/biocLite.R")
install.packages(c("RColorBrewer","mixOmics"))
biocLite("edgeR")
biocLite("limma")
install.packages("stringi")
library(edgeR)
library(limma)
library(RColorBrewer)
library(mixOmics)
library(stringi)

#your folder may be in a different location
setwd("C:/Users/William/Desktop/FeatureCounts_Data/N4_MergedCountData")
rawCountTable <- read.table("N4_MergedCountData.txt", header=TRUE, sep="\t", row.names=1)
sampleInfo <- read.table("N4_design24files.csv", header=TRUE, sep=",", row.names= 1)
head(rawCountTable)
nrow(rawCountTable)

#I'm just loading dplyr to use the select function (CAT)
library(dplyr)

#This is telling R to create a new dataframe without the following columns (CAT)
countData <- select(rawCountTable, -Chr, -Start, -End, -Strand, -Length)

#We then use the new dataframe to build the DGE
dgeFull <- DGEList(countData, group=sampleInfo$Ca.condition,sampleInfo$P.condition)
dgeFull

#We want to log transform our data BUT there are alot of 0s in our dgeFull counts 
#What happens when you take try log2(0) This is why we add +1 (pseudocounts)
pseudoCounts <- log2(dgeFull$counts+1)
head(pseudoCounts)
#You'll see there are a lot of features not hit hit (0)
hist(pseudoCounts[,"PseudoCounts Non-Normalized Gene Expression"])
boxplot(pseudoCounts, col="gray", las=3)
#Is there a difference in overall experession between treatments?

#Lets compare individuals - so called MA-plot
par(mfrow=c(1,3))
## WT1 vs WT2
# A values #Average count of a gene
avalues <- (pseudoCounts[,1] + pseudoCounts[,2])/2
# M values #Log fold change in expression of a gene
mvalues <- (pseudoCounts[,1] - pseudoCounts[,2])
plot(avalues, mvalues, xlab="A", ylab="M", pch=19, main="WT")
abline(h=0, col="red")

## Mt1 vs Mt2
# A values
avalues <- (pseudoCounts[,4] + pseudoCounts[,5])/2
# M values
mvalues <- (pseudoCounts[,4] - pseudoCounts[,5])
plot(avalues, mvalues, xlab="A", ylab="M", pch=19, main="MT")
abline(h=0, col="red")
## WT1 vs Mt2
# A values
avalues <- (pseudoCounts[,1] + pseudoCounts[,4])/2
# M values
mvalues <- (pseudoCounts[,1] - pseudoCounts[,4])
plot(avalues, mvalues, xlab="A", ylab="M", pch=19, main="WT vs MT")
abline(h=0, col="red")
#Similar plots, but much more spread in WT vs MT

par(mfrow=c(1,3))
#Are samples correlated?
plot(pseudoCounts[,1], pseudoCounts[,2])
plot(pseudoCounts[,4], pseudoCounts[,5])
plot(pseudoCounts[,1], pseudoCounts[,4])
#Again, similar plots, but much more spread in WT vs MT


#The purpose of multidimensional scaling (MDS) is to provide a visual representation of the pattern of proximities
#(i.e., similarities or distances) among a set of objects
plotMDS(pseudoCounts)


#Another way to explore the similarities and dissimilarities between samples, it is often instructive to look a clustering image map
#(CIM) or heatmap of sample-to-sample distance matrix.
par(mfrow=c(1,3))
sampleDists <- as.matrix(dist(t(pseudoCounts)))
sampleDists
cimColor <- colorRampPalette(rev(brewer.pal(9, "Blues")))(16)
#RStudio not happy producing this plot, so made a PDF!
pdf()
cim(sampleDists, color=cimColor, symkey=FALSE)
dev.off()
#How do you interpret hthe colour scale? Blue vs white means..

#We can also normalize to counts per million
par(mfrow=c(1,1))
eff.lib.size <- dgeFull$samples$lib.size*dgeFull$samples$norm.factors
normCounts <- cpm(dgeFull)
pseudoNormCounts <- log2(normCounts + 1)
boxplot(pseudoNormCounts, col="gray", las=3)

(Different boxplot than that of what Dr.Shafer made)

#Determining genes that were differentially expressed under the various conditions (https://bioconductor.org/packages/release/bioc/html/DEGseq.html)
source("https://bioconductor.org/biocLite.R")
biocLite("DEGseq")
library(DEGseq)
