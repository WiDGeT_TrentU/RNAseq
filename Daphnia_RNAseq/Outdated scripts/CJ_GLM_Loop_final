#Clear memory first
rm(list = ls())

#Load packages
source("https://bioconductor.org/biocLite.R")
biocLite("edgeR")
install.packages("ggfortify")
install.packages("lmSupport")
library(edgeR)
library(dplyr)
library(ggfortify)
library(lmSupport)
#-----------------------------------------------------------------------------------------
#This is to produce the raw count table for idxstats data (CAT)
files <- dir(pattern="*\\.txt$")
rawCountTable <- readDGE(files, path="~/Uni Stuff/PhD/BioInformatics/featureCounts", 
                         columns=c(1,3),
                         group = NULL, labels = NULL)
#------------------------------------------------------------------------------------------
#This is to produce the raw count table for featurecounts data (WILL)
rawCountTable <- read.table("N3_MergedCountData.txt", header=TRUE, sep="\t", row.names=1)
#This is to give R the treatment groups
sampleInfo <- read.table("N4_design24files.csv", header=TRUE, sep=",", row.names= 1)

#-----------------------------------------------------------------------------------------
#turn the new data into a data frame
new_table<-rawCountTable[,(6:29)]

#enter the treatment groups
groups=as.factor(c("H100","L100","H100","L100","L600","L100",
                   "L100","L100", "L600","L100","L100","H100","L600","L600","L100"
                   ,"L600","L600","L100","H100","L100","L100","H100","L100","H100"))

#Change column names so it's simpler
names(new_table) <- c("H100.1","L100.1","H100.2","L100.2","L600.1","L100.3",
                      "L100.1","L100.4", "L600.2","L100.2","L100.3","H100.3","L600.3","L600.4","L100.4"
                      ,"L600.5","L600.6","L100.5","H100.4","L100.6","L100.5","H100.5","L100.6","H100.6")

#---------------------------------------------------------------------------------------

#Test glm on one gene and run diagnostics

gene1 <- (as.numeric(new_table["DAPPUDRAFT_223325",]))

#Generalised linear regression with a poisson link function
glm_poi <- glm(gene1 ~ groups,family=c("poisson"), data=new_table)

summary(glm_poi)
#Plot the residuals
opar <- par(mfrow = c(2,2), oma = c(0, 0, 1.1, 0))
plot(glm_poi, las = 1)
autoplot(glm_poi)

#--------------------------------------------------------------------------------
#Power analysis

modelPower(pc = 4, pa = 5, N = 24, alpha = 0.05, power = NULL, peta2=0.3025)

#-----------------------------------------------------------------------------------------
#Link function to perform glm on all ~25000 genes then print summary output to 
#a .txt file
loop_list=as.factor(row.names(new_table))
loop_list

for(k in loop_list){
  gene <- as.numeric(new_table[k,])
      k <- glm(gene ~ groups, family = poisson)
      capture.output(summary(k), file=paste0("glm.txt"), append=TRUE)
      
}

#-----------------------------------------------------------------------------------------
#Link function to perform glm on H100 vs L600 genes then print summary output to 
#a .txt file

#First, separate out these data from new_table
H100L600 <- new_table[ , c("H100.1","L600.1", "H100.2","L600.2", "H100.3","L600.3",
                           "H100.4","L600.4", "H100.5","L600.5", "H100.6","L600.6")]

H100L600groups = as.factor(c("H100","L600", "H100","L600", "H100","L600",
                             "H100","L600", "H100","L600", "H100","L600"))

loop_list=as.factor(row.names(H100L600))
loop_list

for(k in loop_list){
  gene <- as.numeric(H100L600[k,])
  k <- glm(gene ~ H100L600groups, family = poisson)
  capture.output(summary(k), file=paste0("glmH100L600.txt"), append=TRUE)
  
}
      
#-----------------------------------------------------------------------------------------
#Link function to perform glm on H100 vs H^00 genes then print summary output to 
#a .txt file

#First, separate out these data from new_table
H100H600 <- new_table[ , c("H100.1","H600.1", "H100.2","H600.2", "H100.3","H600.3",
                           "H100.4","H600.4", "H100.5","H600.5", "H100.6","H600.6")]

H100H600groups = as.factor(c("H100","H600", "H100","H600", "H100","H600",
                             "H100","H600", "H100","H600", "H100","H600"))

loop_list=as.factor(row.names(H100H600))
loop_list

for(k in loop_list){
  gene <- as.numeric(H100H600[k,])
  k <- glm(gene ~ H100H600groups, family = poisson)
  capture.output(summary(k), file=paste0("glmH100H600.txt"), append=TRUE)
  
}

#-----------------------------------------------------------------------------------------
#Link function to perform glm on H100 vs L100 genes then print summary output to 
#a .txt file

#First, separate out these data from new_table
H100L100 <- new_table[ , c("H100.1","L100.1", "H100.2","L100.2", "H100.3","L100.3",
                           "H100.4","L100.4", "H100.5","L100.5", "H100.6","L100.6")]

H100L100groups = as.factor(c("H100","L100", "H100","L100", "H100","L100",
                             "H100","L100", "H100","L100", "H100","L100"))

loop_list=as.factor(row.names(H100L100))
loop_list

for(k in loop_list){
  gene <- as.numeric(H100L100[k,])
  k <- glm(gene ~ H100L100groups, family = poisson)
  capture.output(summary(k), file=paste0("glmH100L100.txt"), append=TRUE)
  
}
