###Performing differential expression testing (with edgeR)
library(edgeR)
library(limma)
library(RColorBrewer)
library(mixOmics)
library(stringi)

#Setting workspace
setwd("C:/Users/William/Desktop/FeatureCounts_Data/N4_MergedCountData/FunRich-Related_Data/H100-L100")
rawCountTable <- read.table("N4_MergedCountData.txt", header=TRUE, sep="\t", row.names=1)
sampleInfo <- read.table("N4_design12files_H100L100.csv", header=TRUE, sep=",", row.names= 1)

#I'm just loading dplyr to use the select function and telling R to create a new dataframe without the following columns
library(dplyr)
countData <- select(rawCountTable, -Chr, -Start, -End, -Strand, -Length)
countData
#Selecting the H100 and L100 countdata in the new dataframe
H100L100CountData <- select(countData,-"L600_Rep1", -"L600_Rep2", -"L600_Rep3", -"L600_Rep4", -"L600_Rep5", -"L600_Rep6", -"H600_Rep1", -"H600_Rep2", -"H600_Rep3", -"H600_Rep4", -"H600_Rep5", -"H600_Rep6")
H100L100CountData

#We then use the new dataframe to build the DGE
dgeFull <- DGEList(H100L100CountData, group=sampleInfo$Ca.condition,sampleInfo$P.condition)

#Making the model matrix to give me a factor variable
design <- model.matrix(~dgeFull$samples$group)
design
dgeFull <- calcNormFactors(dgeFull)
dgeFull <- estimateDisp(dgeFull, design = design)

#This is just the same generalised linear model that was used in generating the DE genes from the RNAseq data
fit <- glmFit(dgeFull, design)
lrt <- glmLRT(fit, coef=ncol(design))
tt <- topTags(lrt, n=nrow(dgeFull), p.value=0.1)

#This is where I apply the BH adjustment to all rows of the tt dataset and output a new adjusted dataset called TT
TT <- topTags(tt, n=nrow(dgeFull),adjust.method="BH", sort.by="PValue", p.value=0.1)
TT

#Just getting rid of the na's from the new dataset
res <- as.data.frame(TT)
is.na(res)

#Multiple testing (Putting the threshold p-value<0.05 to remove any null hypotheses from experimental errors and the differentially expressed genes are due to the treatments)
sum(res$PValue<0.05, na.rm=TRUE)
sum(!is.na(res$PValue<0.05))

#Determining signifcantly differentially expressed (DE) genes with the corrected/adjusted p.value (Determned by taking 0.05 and divide by the amount of test (genes that were tested with the p.value of 0.05, which was 0.05/33049 = 0.0000015129))
ttadjust <- topTags(TT, n=nrow(dgeFull), p.value=0.0000015129)
ttadjust

#Exporting the DE genes in 'ttadjust' into a .csv file (Saving as a xlsx file)
install.packages("xlsx")
library("xlsx")
write.xlsx(ttadjust, file="C:/Users/William/Desktop/FeatureCounts_Data/N4_MergedCountData/FunRich-Related_Data/H100-L100/H100L100FunRich-Related_Data.xlsx", sheetName="DATA", col.names=TRUE, row.names=TRUE, append=FALSE)

##Making boxplots of the genes that were differentially expressed (loop function)
#list of genes and pvalues as you have in your script - use tt at 0.05 until we figure it out
TT
rawCountTable
new_table<-select(rawCountTable, -Chr, -Start, -End, -Strand, -Length, -"L600_Rep1", -"L600_Rep2", -"L600_Rep3", -"L600_Rep4", -"L600_Rep5", -"L600_Rep6", -"H600_Rep1", -"H600_Rep2", -"H600_Rep3", -"H600_Rep4", -"H600_Rep5", -"H600_Rep6")
groups=as.factor(c("H100","H100","H100","H100","H100","H100","L100","L100","L100","L100","L100","L100"))

#list to loop over
loop_list=as.factor(row.names(TT))
for(i in loop_list){
  gene <- as.numeric(new_table[i,])
  pdf(i)
  boxplot(gene~groups,main=i)
  dev.off()
}

